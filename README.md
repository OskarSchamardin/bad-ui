# Bad-ui

This is an example of intentionally bad UI design. It prompts the user to
submit their phone number to a service, but the user interface is an old
school dumbphone - inputting a single number takes multiple keystrokes to
achieve.

I cannot gaurentee the accuracy of the phone's behaviour, as I may remember
dumbphones differently than the actually were.

[Link to live demo](https://oskarschamardin.gitlab.io/bad-ui)

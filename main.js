/* Ths is the main ja code in the project */

let number = "";                /* user's phone number. */
let buttonLog = [];             /* log of recently pressed buttons. */
let numberRepeatedCount = 0;    /* number of times last button has been clicked. */
let nextLetterTimeout = null;   /* timeout variable used with letter input. */

/* Initialize the audio part of the project */
let audioContext = new(window.AudioContext || window.webkitAudioContext)();
let gainNode = audioContext.createGain();

function kbInput(input) {
    /* function called when a keypad button gets clicked */

    /* get tones from object (keypadValues.js) */
    let toneHorizontal = keypadValues[keypadValues.map(function(e) {return e.name}).indexOf(input)].toneHorizontal;
    let toneVertical = keypadValues[keypadValues.map(function(e) {return e.name}).indexOf(input)].toneVertical;

    /* generate and play DTMF tone */
    if (toneHorizontal !== null && toneVertical !== null) {
        playTone(toneHorizontal, 150);
        playTone(toneVertical, 150);
    }

    /* when a number button has been clicked */
    if (typeof(input) === 'number') {
        if (input === 0 || input === 1) {
            /* Input '0' or '1' directly because they don't have letters */
            appendToNumber(input);
        } else {
            clearTimeout(nextLetterTimeout);
            buttonLog.push(input);
            numberWithLetter(input);
        }

        /* when a special button has been clicked */
    } else {
        switch (input) {
            case 'del':
                deleteFromNumber();
                break;
            case 'sub':
                /* test if number is indeed a number with regex */
                (/^\d+$/.test(number)) ? alert(`Your phone number is: ${number}\nSUCCESS!`) : alert(`Phone number contains non digit characters, please try again.`);
                break;
            //case 'menu':
            //    alert(input);
            //    break;
            //case 'asterisk':
            //    alert(input);
            //    break;
            //case 'hash':
            //    alert(input);
            //    break;
        }
    }
}

function numberWithLetter(input) {
    /* simulates clicking a keypad button with letters */

    let lastButtonClicked = buttonLog[buttonLog.length - 2];

    /* get actualValue for key */
    let actualKeyValue = keypadValues[keypadValues.map(function(e) {return e.name}).indexOf(input)].actualValue;

    try { clearTimeout(nextLetterTimeout) } catch {  }

    /* time given to push current button again or its value will be appended */
    nextLetterTimeout = setTimeout(function() {
        numberRepeatedCount = 0;
        buttonLog = [];
    }, 1500);

    if (buttonLog.length > 1) {
        if (lastButtonClicked === input) {
            numberRepeatedCount++;

            /* restart count if overflow */
            if (numberRepeatedCount - 1 >= actualKeyValue.length) {
                numberRepeatedCount = 1;
            }

            deleteFromNumber();
            appendToNumber(actualKeyValue[numberRepeatedCount - 1]);
        } else {
            numberRepeatedCount = 0;
            buttonLog = [];
            buttonLog[0] = input;

            appendToNumber(actualKeyValue[numberRepeatedCount]);
            numberRepeatedCount++;
        }
    } else {
        appendToNumber(actualKeyValue[numberRepeatedCount]);
        numberRepeatedCount++;
    }
}

function appendToNumber(input) {
    /* adds a character to the number field on phone screen */

    let numberField = document.getElementById('number').innerHTML;

    number += input.toString();

    numberField = `&gt ${number}`;
    document.getElementById('number').innerHTML = numberField;
}

function deleteFromNumber() {
    /* removes a character from the end of the number field on phone screen */

    let numberField = document.getElementById('number').innerHTML;

    if (number.length !== 0) {
        /* remove last character */
        number = number.slice(0, -1);
    } else {
        return;
    }

    numberField = `&gt ${number}`
    document.getElementById('number').innerHTML = numberField;
}

function playTone(frequency, duration) {
    /* generates and plays a tone when a keypad key is clicked
     * Tones reference: https://en.wikipedia.org/wiki/Telephone_keypad#Key_tones
     * Call this function twice to hear both tones at once.
     */

    let oscillator = audioContext.createOscillator();

    oscillator.type = 'sine';
    oscillator.frequency.value = frequency;
    oscillator.connect(gainNode);
    gainNode.connect(audioContext.destination);

    gainNode.gain.value = 0.1; /* Set volume (be careful not to blast out your ears) */

    oscillator.start();

    setTimeout(function() {
        oscillator.stop();
    }, duration);
}
